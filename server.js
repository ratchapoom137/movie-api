'use strict'

const express = require('express')
const app = express()

const port = 8000;

app.get('/movies', (req, res) => {
    const result = {
        results: [{
            title: 'fake title 1',
            image_url: 'fake image url 1',
            overview: 'fake overview 1'
        }, {
            title: 'fake title 2',
            image_url: 'fake image url 2',
            overview: 'fake overview 2'
        }]
    }

    res.json(result)
})

app.listen(port, () => 
    console.log(`start app ${port}`))


